//
//  DSArtistDataSource.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 04/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSArtistDataSource.h"
#import "DSArtistCell.h"

@interface DSArtistDataSource()

@property (strong, nonatomic) NSMutableArray *artists;

@end

@implementation DSArtistDataSource

#pragma mark - Init

- (instancetype)initWithArtists:(NSArray *)artists {
    self = [super init];
    if (self) {
        self.artists = [NSMutableArray arrayWithArray:artists];
    }
    return self;
}

#pragma mark - Public

- (void)appendArtists:(NSArray *)artists {
    [self.artists addObjectsFromArray:artists];
}

- (void)removeAllObjects {
    [self.artists removeAllObjects];
}

- (DSArtist *)artistWithIndexPath:(NSIndexPath *)indexPath {
    return [self.artists objectAtIndex:indexPath.row];
}

- (NSUInteger)count {
    return self.artists.count;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.artists.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    DSArtistCell *artistCell = [tableView dequeueReusableCellWithIdentifier:@"CellIdentifier" forIndexPath:indexPath];
    DSArtist *artist = [self.artists objectAtIndex:indexPath.row];
    [artistCell configureCellWith:artist.name imgURL:artist.picture_medium];
    
    return artistCell;
}

@end
