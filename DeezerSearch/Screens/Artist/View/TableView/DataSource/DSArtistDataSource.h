//
//  DSArtistDataSource.h
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 04/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DSArtist.h"

@interface DSArtistDataSource : NSObject <UITableViewDataSource>

@property (readonly) NSUInteger count;

- (instancetype)initWithArtists:(NSArray *)artists;
- (DSArtist *)artistWithIndexPath:(NSIndexPath *)indexPath;
- (void)appendArtists:(NSArray *)artists;
- (void)removeAllObjects;

@end
