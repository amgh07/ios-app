//
//  DSArtistCell.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 04/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSArtistCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface DSArtistCell ()
@property (weak, nonatomic) IBOutlet UIImageView *artistImageView;
@property (weak, nonatomic) IBOutlet UILabel *artistLabel;

@end

@implementation DSArtistCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.contentView setBackgroundColor:[UIColor defaultColor]];
    
    UIView *bgColorView = [UIView new];
    bgColorView.backgroundColor = [UIColor defaultLightColor];
    [self setSelectedBackgroundView:bgColorView];
    
}

- (void)configureCellWith:(NSString *)name imgURL:(NSString *)imgURL {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.artistImageView sd_setImageWithURL:[NSURL URLWithString:imgURL] placeholderImage:nil options:0 completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            [UIView transitionWithView:self.artistImageView
                              duration:0.2f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                [self.artistImageView setImage:image];
                            } completion:NULL];
            
        }];
    });
    [self.artistLabel setText:name];
}

@end
