//
//  DSArtistTableView.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 06/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSArtistTableView.h"
#import "DSArtistTableViewHeader.h"

@interface DSArtistTableView ()

@end

@implementation DSArtistTableView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self registerNibForArtistCell];
    
    [self setBackgroundColor:[UIColor defaultColor]];
    [self setSeparatorColor:[UIColor blackColor]];
    
    DSArtistTableViewHeader *artistTableViewHeader = [[DSArtistTableViewHeader alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width , 50)];
    
    [self setContentOffset:CGPointMake(0,50)];
    [self setTableHeaderView:artistTableViewHeader];
    [self setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
}

@end
