//
//  DSArtistController.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 04/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSArtistController.h"
#import "DSArtistPresenter.h"
#import "DSArtistDataSource.h"
#import "DSAlbumController.h"
#import "DSPlaceholderView.h"
#import "DSSearchBar.h"
#import "DSArtistTableView.h"

@interface DSArtistController () <UITableViewDelegate, UIScrollViewDelegate, UISearchBarDelegate,  DSQueryView>

@property (weak, nonatomic) IBOutlet DSSearchBar *searchBar;
@property (weak, nonatomic) IBOutlet DSArtistTableView *tableView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet DSPlaceholderView *emptyArtistView;
@property (strong, nonatomic) DSArtistPresenter *artistPresenter;
@property (strong, nonatomic) DSArtistDataSource *artistDataSource;

@end

@implementation DSArtistController

- (instancetype)init {
    return [super initWithNibName:NSStringFromClass([self class]) bundle:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationItem setTitle:@"ARTIST"];
    [self.tableView setDelegate:self];
    self.searchBar.delegate = self;
    self.artistPresenter = [[DSArtistPresenter alloc] initWithQueryView:self];
    [self hideEmptyView:NO];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self hideKeyBoard];
}

- (void)viewDidLayoutSubviews {
    [self.tableView setDefaultSeparatorAndLayoutMargin];
}

#pragma mark - DSArtistView

- (void)viewWillQuery {
    [self indicateActivity:YES];
}

- (void)viewDidQuery {
    [self indicateActivity:NO];
}

- (void)viewDidQueryWithResult:(NSArray *)result append:(BOOL)append {
    DSArtistController  *__weak weakSelf = self;
    [super dispatch_async_main_queue:^{
        [weakSelf loadQueryResult:result append:append];
    }];
}

- (void)viewDidQueryEmptyResult {
    [self hideEmptyViewWithType:NO typ:DSEmptyViewTypeSad memessage:@"Ouch!\nNothing to find here..."];
}

- (void)viewDidFailedToQuery {
    [self hideEmptyViewWithType:NO typ:DSEmptyViewTypeConnection memessage:@"Ouch!\nNo connection..."];
}

#pragma mark - Helpers

- (void)loadQueryResult:(NSArray *)result append:(BOOL)append {
    if (!self.artistDataSource) {
        self.artistDataSource = [[DSArtistDataSource alloc] initWithArtists:result];
        self.tableView.dataSource = self.artistDataSource;
    } else {
        if (!append) {
            [self.artistDataSource removeAllObjects];
        }
        [self.artistDataSource appendArtists:result];
    }
    [self hideEmptyView:YES];
    [self.tableView reloadData];
}

- (void)indicateActivity:(BOOL)activ {
    [super dispatch_async_main_queue:^{
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:activ];
        if (activ) {
            [self.activityIndicator startAnimating];
        } else {
            [self.activityIndicator stopAnimating];
        }
    }];
}

- (void)queryArtistsWithQuery:(NSString *)query newQuery:(BOOL)newQuery {
    [self.artistPresenter queryArtists:query newQuery:newQuery];
}

- (void)hideEmptyViewWithType:(BOOL)hide typ:(DSEmptyViewType)typ memessage:(NSString *)message {
    [super dispatch_async_main_queue:^{
        [self.emptyArtistView setHidden:hide];
        [self.emptyArtistView configureWithViewType:typ message:message];
        [self.tableView setHidden:!hide];
    }];
}

- (void)hideEmptyView:(BOOL)hide   {
    [self hideEmptyViewWithType:hide typ:DSEmptyViewTypeHappy memessage:@"Hi there!"];
}

- (void)hideKeyBoard {
    [self.view endEditing:YES];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self hideKeyBoard];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 75;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    DSArtist *artist = [self.artistDataSource artistWithIndexPath:indexPath];
    DSAlbumController *albumController = [[DSAlbumController alloc] initWithQuery:artist.name];
    UIBarButtonItem *buttonItem = [UIBarButtonItem new];
    [buttonItem setTitle:@""];
    [self.navigationItem setBackBarButtonItem:buttonItem];
    [self.view endEditing:YES];
    [self.navigationController pushViewController:albumController animated:YES];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ((indexPath.row) == self.artistDataSource.count - 1) { //The last one!
        [self queryArtistsWithQuery:self.searchBar.text newQuery:NO];
    }
}

#pragma mark - UISearchBarDelegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if ([searchText length] == 0) {
        [self.artistPresenter cancelAllPendingQueries];
        [self.artistDataSource removeAllObjects];
        [self.tableView reloadData];
        [self hideEmptyView:NO];
    }
}

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {

    NSString *searchBarTextToBecome = [searchBar.text stringByReplacingCharactersInRange:range withString:text];
    if (![DSUtils allowMergeOfStringInRange:range stringToMerge:text stringAfterMerge:searchBarTextToBecome currentString:[searchBar textField].text]) {
        return NO;
    }
    NSString *query = nil;
    //Query while user types inn
    if ([searchBarTextToBecome length] >= 1 && ![searchBarTextToBecome lastCharacterIsMemberOfWhitespaceCharacterSet] && ![text isEmpty]) {
        query = searchBarTextToBecome;
    }
    //Query back space
    if ([text isEmpty]) {
        if ([searchBarTextToBecome lastCharacterIsMemberOfWhitespaceCharacterSet]) {
            query = [searchBarTextToBecome stringByTrimmingWhitespaceCharacterSet];
        }
        if (![searchBarTextToBecome isEmpty] && ![searchBarTextToBecome lastCharacterIsMemberOfWhitespaceCharacterSet] && ![searchBar.textField.text lastCharacterIsMemberOfWhitespaceCharacterSet]) {
            query = searchBarTextToBecome;
        }
    }
    if (query) {
        [self queryArtistsWithQuery:query newQuery:YES];
    }
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self hideKeyBoard];
}

@end
