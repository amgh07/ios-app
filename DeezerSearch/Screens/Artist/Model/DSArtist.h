//
//  DSArtist.h
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 04/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface DSArtist : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *picture_medium;

@end
