//
//  DSArtistPresenter.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 04/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSArtistPresenter.h"
#import "DSArtistService.h"

@implementation DSArtistPresenter

- (instancetype)initWithQueryView:(id<DSQueryView>)queryView {
    return [super initWithQueryView:queryView service:[DSArtistService new]];
}
- (void)queryArtists:(NSString *)query newQuery:(BOOL)newQuery {
    [super query:query newQuery:newQuery];
}

@end
