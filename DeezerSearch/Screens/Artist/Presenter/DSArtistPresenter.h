//
//  DSArtistPresenter.h
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 04/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSQueryPresenter.h"
#import "DSQueryView.h"

@interface DSArtistPresenter : DSQueryPresenter

- (instancetype)initWithQueryView:(id<DSQueryView>)queryView;
- (void)queryArtists:(NSString *)query newQuery:(BOOL)newQuery;

@end
