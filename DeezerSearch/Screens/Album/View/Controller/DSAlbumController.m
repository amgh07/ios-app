//
//  DSAlbumController.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 04/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSAlbumController.h"
#import "DSAlbumPresenter.h"
#import "DSAlbumDataSource.h"
#import "DSQueryView.h"
#import "DSSearchBar.h"
#import "DSAlbumCollectionView.h"
#import "DSTrackController.h"
#import "DSPlaceholderView.h"

@interface DSAlbumController () <UICollectionViewDelegate, DSQueryView>

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet DSSearchBar *searchBar;
@property (weak, nonatomic) IBOutlet DSAlbumCollectionView *collectionView;
@property (weak, nonatomic) IBOutlet DSPlaceholderView *emptyView;
@property (strong, nonatomic) DSAlbumPresenter *albumPresenter;
@property (strong, nonatomic) DSAlbumDataSource *albumDataSource;
@property (strong, nonatomic) NSString *query;

@end

@implementation DSAlbumController

- (instancetype)initWithQuery:(NSString *)query {
    self = [super initWithNibName:NSStringFromClass([self class]) bundle:nil];
    if (self) {
        [self setQuery:query];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [super dispatch_async_main_queue:^{
        [self setAlbumPresenter:[[DSAlbumPresenter alloc] initWithQueryView:self]];
        [self.albumPresenter queryAlbums:self.query newQuery:YES];
        [self.activityIndicator startAnimating];
    }];
    [self.navigationItem setTitle:@"ALBUMS"];
    [self.collectionView setDelegate:self];
    [self.searchBar setUserInteractionEnabled:NO];
    [self.searchBar.textField setClearButtonMode:UITextFieldViewModeNever];
    [self.searchBar setText:self.query];
}

#pragma mark - DSQueryView

- (void)viewDidQueryWithResult:(NSArray *)result append:(BOOL)append {
    DSAlbumController  *__weak weakSelf = self;
    [super dispatch_async_main_queue:^{
        [weakSelf loadQueryResult:result append:append];
    }];
}

- (void)loadQueryResult:(NSArray *)result append:(BOOL)append {
    [super dispatch_async_main_queue:^{
        if (!self.albumDataSource) {
            self.albumDataSource = [[DSAlbumDataSource alloc] initWithAlbums:result];
            [self.collectionView setDataSource:self.albumDataSource];
            [self.activityIndicator stopAnimating];
        } else {
            [self.albumDataSource appendAlbums:result];
            [self.collectionView reloadData];
        }
    }];
}

- (void)viewDidQueryEmptyResult {
    DSAlbumController  *__weak weakSelf = self;
    [super dispatch_async_main_queue:^{
        [weakSelf showEmptyViewWithType:DSEmptyViewTypeSad message:@"Ouch!\nNothing to find here..."];
    }];
}

- (void)viewDidFailedToQuery {
    DSAlbumController  *__weak weakSelf = self;
    [super dispatch_async_main_queue:^{
        [weakSelf showEmptyViewWithType:DSEmptyViewTypeConnection message:@"Ouch!\nNo connection..."];
    }];
}

#pragma mark - Helpers

- (void)showEmptyViewWithType:(DSEmptyViewType)typ message:(NSString *)message {
    [self.emptyView setHidden:NO];
    [self.emptyView configureWithViewType:typ message:message];
    [self.activityIndicator stopAnimating];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self hideKeyBoard];
}

- (void)hideKeyBoard {
    [self.view endEditing:YES];
}

#pragma mark - UICollectionViewDelegate

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    CGFloat paddingSpace =  UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0).left * 3;
    CGFloat availableWidth = [UIScreen mainScreen].bounds.size.width - paddingSpace;
    CGFloat widthPerItem = availableWidth / 2;
    
    return CGSizeMake(widthPerItem, widthPerItem + 40);
    
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5.0, 10.0, 10.0, 10.0);
    
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return  UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0).left;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath NS_AVAILABLE_IOS(8_0) {
    if (indexPath.section == 0 && (indexPath.row + 10) == self.albumDataSource.count) { //Just 10 cells before the last one!
        [self.albumPresenter queryAlbums:self.query newQuery:NO];
    }
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    DSAlbum *album = [self.albumDataSource albumWithIndexPath:indexPath];
    DSTrackController *trackController = [[DSTrackController alloc] initWithAlbum:album];
    [self.navigationController pushViewController:trackController animated:YES];
}

@end
