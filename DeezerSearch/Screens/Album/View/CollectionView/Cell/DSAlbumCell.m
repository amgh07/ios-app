//
//  DSAlbumCell.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 05/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSAlbumCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface DSAlbumCell ()

@property (weak, nonatomic) IBOutlet UIImageView *albumImageView;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *name;

@end

@implementation DSAlbumCell

NSString *const kDSAlbumCellIdentifier = @"kDSAlbumCellIdentifier"; 

- (void)awakeFromNib {
    [super awakeFromNib];

    UIView *bgColorView = [UIView new];
    bgColorView.backgroundColor = [UIColor defaultLightColor];
    [self setSelectedBackgroundView:bgColorView];
}

- (void)configureWithImgURL:(NSString *)imgURL name:(NSString *)name title:(NSString *)title {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.albumImageView sd_setImageWithURL:[NSURL URLWithString:imgURL] placeholderImage:nil options:0 completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
            [UIView transitionWithView:self.albumImageView
                              duration:0.1f
                               options:UIViewAnimationOptionTransitionCrossDissolve
                            animations:^{
                                [self.albumImageView setImage:image];
                                if (error) {
                                    [self.albumImageView setImage:[UIImage imageNamed:@"close_red"]];
                                }
                            } completion:NULL];
            
        }];
    });
    
    [self.title setText:title];
    [self.name setText:name];
}

@end
