//
//  DSAlbumCollectionView.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 06/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSAlbumCollectionView.h"
#import "DSAlbumCell.h"

@implementation DSAlbumCollectionView

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setBackgroundColor:[UIColor defaultColor]];
    [self registerNib:[UINib nibWithNibName:NSStringFromClass([DSAlbumCell class]) bundle:nil] forCellWithReuseIdentifier:kDSAlbumCellIdentifier];
}

@end
