//
//  DSAlbumDataSource.h
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 05/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import <UIKit/UICollectionView.h>
#import "DSAlbum.h"

@interface DSAlbumDataSource : NSObject <UICollectionViewDataSource>
@property (readonly) NSUInteger count;

- (instancetype)initWithAlbums:(NSArray *)albums;
- (DSAlbum *)albumWithIndexPath:(NSIndexPath *)indexPath;
- (void)appendAlbums:(NSArray *)albums;

@end
