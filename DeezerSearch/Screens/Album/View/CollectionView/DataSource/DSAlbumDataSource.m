//
//  DSAlbumDataSource.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 05/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSAlbumDataSource.h"
#import "DSAlbumCell.h"

@interface DSAlbumDataSource ()

@property (strong, nonatomic) NSMutableArray *albums;

@end

@implementation DSAlbumDataSource

- (instancetype)initWithAlbums:(NSArray *)albums {
    self = [super init];
    if (self) {
        self.albums = [NSMutableArray arrayWithArray:albums];
    }
    return self;
}

- (DSAlbum *)albumWithIndexPath:(NSIndexPath *)indexPath {
    return [self.albums objectAtIndex:indexPath.row];
}

- (void)appendAlbums:(NSArray *)albums {
    [self.albums addObjectsFromArray:albums];
}

- (void)removeAllObjects {
    [self.albums removeAllObjects];
}

- (NSUInteger)count {
    return self.albums.count;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.albums.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    DSAlbumCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kDSAlbumCellIdentifier forIndexPath:indexPath];
    DSAlbum *artist = [self.albums objectAtIndex:indexPath.row];
    [cell configureWithImgURL:artist.cover_big name:artist.artist.name title:artist.title];
    return cell;
}

@end
