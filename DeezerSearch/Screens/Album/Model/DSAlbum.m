//
//  DSAlbum.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 04/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSAlbum.h"

@implementation DSAlbum

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"aid": @"id",
             @"title": @"title",
             @"cover_big": @"cover_big",
             @"cover_xl": @"cover_xl",
             @"artist": @"artist"
             };
}

@end
