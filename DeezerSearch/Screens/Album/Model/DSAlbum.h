//
//  DSAlbum.h
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 04/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSArtist.h"

@interface DSAlbum : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSNumber *aid;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *cover_big;
@property (nonatomic, strong) NSString *cover_xl;
@property (nonatomic, strong) DSArtist *artist;

@end
