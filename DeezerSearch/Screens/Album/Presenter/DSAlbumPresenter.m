//
//  DSAlbumPresenter.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 04/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSAlbumPresenter.h"
#import "DSAlbumService.h"

@implementation DSAlbumPresenter

- (instancetype)initWithQueryView:(id<DSQueryView>)queryView {
    return [super initWithQueryView:queryView service:[DSAlbumService new]];
}

- (void)queryAlbums:(NSString *)query newQuery:(BOOL)newQuery {
    [super query:query newQuery:newQuery];
}

@end
