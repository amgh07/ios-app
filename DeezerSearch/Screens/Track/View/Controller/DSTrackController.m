//
//  DSTrackController.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 04/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSTrackController.h"
#import "DSTrackDataSource.h"
#import "DSTrackPresenter.h"
#import "DSTrackTableView.h"
#import "DSTrackTableSectionHeader.h"
#import "DSQueryView.h"
#import "DSPlaceholderView.h"

@interface DSTrackController () <DSQueryView, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet DSTrackTableView *tableView;
@property (weak, nonatomic) IBOutlet DSPlaceholderView *emptyView;
@property (strong, nonatomic) DSTrackDataSource *trackDataSource;
@property (strong, nonatomic) DSTrackPresenter *trackPresenter;
@property (strong, nonatomic) DSAlbum *album;

@end

@implementation DSTrackController

- (instancetype)initWithAlbum:(DSAlbum *)album {
    self = [super initWithNibName:@"DSTrackController" bundle:nil];
    if (self) {
        self.album = album;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [super dispatch_async_main_queue:^{
        [self.tableView configureHeader:self.album.cover_xl];
        [self setTrackPresenter:[[DSTrackPresenter alloc] initWithQueryView:self]];
        [self.trackPresenter queryTracks:[self.album.aid stringValue]];
    }];
    
    self.tableView.delegate = self;
}

- (void)viewDidLayoutSubviews {
    [self.tableView setDefaultSeparatorAndLayoutMargin];
}

#pragma mark - DSTrackView

- (void)viewDidQueryWithResult:(NSArray *)result append:(BOOL)append {
    DSTrackController  *__weak weakSelf = self;
    [super dispatch_async_main_queue:^{
        [weakSelf loadQueryResult:result append:append];
    }];
}

- (void)viewDidQueryEmptyResult {
    DSTrackController  *__weak weakSelf = self;
    [super dispatch_async_main_queue:^{
        [weakSelf showEmptyViewWithType:DSEmptyViewTypeSad message:@"Ouch!\nNothing to find here..."];
    }];
}

- (void)viewDidFailedToQuery {
    DSTrackController  *__weak weakSelf = self;
    [super dispatch_async_main_queue:^{
        [weakSelf showEmptyViewWithType:DSEmptyViewTypeConnection message:@"Ouch!\nNo connection..."];
    }];
}

#pragma mark - Helpers

- (void)loadQueryResult:(NSArray *)result append:(BOOL)append {
    if (!self.trackDataSource) {
        self.trackDataSource = [[DSTrackDataSource alloc] initWithTracks:result];
        self.tableView.dataSource = self.trackDataSource;
    } else {
        [self.trackDataSource appendTracks:result];
    }
    [self.tableView reloadData];
}

- (void)showEmptyViewWithType:(DSEmptyViewType)typ message:(NSString *)message {
    [self.emptyView setHidden:NO];
    [self.emptyView configureWithViewType:typ message:message];
    [self.tableView setHidden:YES];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 35.0f;
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    DSTrackTableSectionHeader *header = (DSTrackTableSectionHeader *)view;
    if (header) {
        [header configureTitle:[self.trackDataSource tableView:tableView titleForHeaderInSection:section]];
    }
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [[DSTrackTableSectionHeader alloc] init];
}

@end
