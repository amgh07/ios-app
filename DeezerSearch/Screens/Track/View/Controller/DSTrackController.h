//
//  DSTrackController.h
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 04/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSBaseController.h"
#import "DSAlbum.h"

@interface DSTrackController : DSBaseController

- (instancetype)initWithAlbum:(DSAlbum *)album;

@end
