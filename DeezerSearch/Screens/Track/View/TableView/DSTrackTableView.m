//
//  DSTrackTableView.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 06/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSTrackTableView.h"
#import "DSTrackTableViewHeader.h"

@interface DSTrackTableView ()

@property (strong, nonatomic) DSTrackTableViewHeader *header;

@end

@implementation DSTrackTableView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self registerNibForTrackCell];
    
    [self setBackgroundColor:[UIColor defaultColor]];
    [self setSeparatorColor:[UIColor blackColor]];

    self.header = [[DSTrackTableViewHeader alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width , [UIScreen mainScreen].bounds.size.height/1.8)];
    
    [self setTableHeaderView:self.header];
    [self setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    
    
}

- (void)configureHeader:(NSString *)imgUrl {
    [self.header configure:imgUrl];
}
@end
