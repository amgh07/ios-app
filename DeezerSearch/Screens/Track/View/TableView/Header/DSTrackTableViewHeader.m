//
//  DSTrackTableViewHeader.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 08/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSTrackTableViewHeader.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface DSTrackTableViewHeader ()

@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIImageView *albumCover;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@end

@implementation DSTrackTableViewHeader

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initView];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView {
    
    [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
    [self addSubview:self.contentView];
    self.contentView.frame = self.bounds;
    [self setBackgroundColor:[UIColor defaultColor]];
    [self.contentView setBackgroundColor:[UIColor defaultLightColor]];
}

- (void)configure:(NSString *)url {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.albumCover sd_setImageWithURL:[NSURL URLWithString:url] placeholderImage:nil options:0 completed:^(UIImage * _Nullable image, NSError * _Nullable error, SDImageCacheType cacheType, NSURL * _Nullable imageURL) {
        [UIView transitionWithView:self.albumCover
                          duration:0.2f
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            [self.albumCover setImage:image];
                            [self.activityIndicator stopAnimating];
                            if (error) {
                                [self.albumCover setImage:[UIImage imageNamed:@"close_red"]];
                            }
                        } completion:NULL];
        }];
    });
}

@end
