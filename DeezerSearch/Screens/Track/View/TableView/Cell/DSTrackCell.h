//
//  DSTrackCell.h
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 06/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DSTrackCell : UITableViewCell

- (void)configurePosition:(NSNumber *)position title:(NSString *)title name:(NSString *)name duration:(NSNumber *)duration;

@end
