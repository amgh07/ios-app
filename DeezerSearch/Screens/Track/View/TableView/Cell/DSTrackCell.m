//
//  DSTrackCell.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 06/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSTrackCell.h"

@interface DSTrackCell ()

@property (weak, nonatomic) IBOutlet UILabel *trackPosition;
@property (weak, nonatomic) IBOutlet UILabel *trackName;
@property (weak, nonatomic) IBOutlet UILabel *trackArtist;
@property (weak, nonatomic) IBOutlet UILabel *trackDuration;


@end

@implementation DSTrackCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.contentView setBackgroundColor:[UIColor defaultColor]];
    [self setBackgroundColor:[UIColor defaultColor]];
    
    UIView *bgColorView = [UIView new];
    bgColorView.backgroundColor = [UIColor defaultLightColor];
    [self setSelectedBackgroundView:bgColorView];

    [self colorDurationAndNameWithColor:[UIColor secondaryLightColor]];
    [self colorPositionAndNameWithColor:[UIColor whiteColor]];
}

- (void)colorPositionAndNameWithColor:(UIColor *)color {
    [self.trackPosition setTextColor:[UIColor whiteColor]];
    [self.trackName setTextColor:[UIColor whiteColor]];
}
- (void)colorDurationAndNameWithColor:(UIColor *)color {
    [self.trackArtist setTextColor:color];
    [self.trackDuration setTextColor:color];
}

- (void)configurePosition:(NSNumber *)position title:(NSString *)title name:(NSString *)name duration:(NSNumber *)duration {
    [self.trackPosition setText:[NSString stringWithFormat:@"%@.", [position stringValue]]];
    [self.trackName setText:title];
    [self.trackArtist setText:name];
    [self.trackDuration setText:[self formatTrackDuration:duration]];
}

- (NSString *)formatTrackDuration:(NSNumber *)duration {
    return [[NSDateFormatter dateFormatter] stringFromDate:[NSDate dateWithTimeIntervalSince1970:[duration integerValue]]];
}

@end
