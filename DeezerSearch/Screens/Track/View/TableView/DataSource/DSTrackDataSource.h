//
//  DSTrackDataSource.h
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 06/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import <UIKit/UITableView.h>

@interface DSTrackDataSource : NSObject <UITableViewDataSource>

- (instancetype)initWithTracks:(NSArray *)tracks;
- (void)appendTracks:(NSArray *)tracks;

@end
