//
//  DSTrackDataSource.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 06/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSTrackDataSource.h"
#import "DSTrackCell.h"
#import "DSTrack.h"

@interface DSTrackDataSource ()

@property (strong, nonatomic) NSArray <NSMutableArray *> *tracks;

@end

@implementation DSTrackDataSource

- (instancetype)initWithTracks:(NSArray *)tracks {
    self = [super init];
    if (self) {
        
        NSPredicate *volum1 = [NSPredicate predicateWithFormat:@"disk_number == %@", @1];
        NSPredicate *volum2 = [NSPredicate predicateWithFormat:@"disk_number >= %@", @2];
        
        NSArray *volum1Array = [tracks filteredArrayUsingPredicate:volum1];
        NSArray *volum2Array = [tracks filteredArrayUsingPredicate:volum2];
        
        
        if (volum2Array.count == 0) {
            //Just one Volume no modification needed.
            self.tracks = [NSArray arrayWithObjects:[NSMutableArray arrayWithArray:volum1Array], nil];
        } else {
            self.tracks = [NSArray arrayWithObjects:[NSMutableArray arrayWithArray:volum1Array], [NSMutableArray arrayWithArray:volum2Array], nil];
        }
    }
    return self;
}

- (void)appendTracks:(NSArray *)tracks {
    [[self.tracks objectAtIndex:self.tracks.count - 1] addObjectsFromArray:tracks];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.tracks.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.tracks objectAtIndex:section].count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    DSTrackCell *trackCell = [tableView dequeueReusableCellWithIdentifier:@"TrackCellIdentifier" forIndexPath:indexPath];
    DSTrack *track = [[self.tracks objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    [trackCell configurePosition:track.track_position title:track.title_short name:track.artist.name duration:track.duration];
    
    return trackCell;
    
}
- (nullable NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [NSString stringWithFormat:@"Volume %@", [NSNumber numberWithInteger:(self.tracks.count + section + 1) - (self.tracks.count)]];
}

@end
