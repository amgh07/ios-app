//
//  DSTrackTableSectionHeader.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 08/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSTrackTableSectionHeader.h"

@interface DSTrackTableSectionHeader ()

@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UILabel *sectionTitle;

@end

@implementation DSTrackTableSectionHeader

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initView];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView {
    [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
    [self addSubview:self.contentView];
    self.contentView.frame = self.bounds;
    
    [self.contentView setBackgroundColor:[UIColor defaultDarkColor]];
}

- (void)configureTitle:(NSString *)title {
    [self.sectionTitle setText:title];
}

@end
