//
//  DSTrack.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 04/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSTrack.h"

@implementation DSTrack

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"tid": @"id",
             @"title": @"title",
             @"title_short": @"title_short",
             @"duration": @"duration",
             @"track_position": @"track_position",
             @"disk_number": @"disk_number",
             @"artist": @"artist"
             };
}

@end
