//
//  DSTrack.h
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 04/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSArtist.h"

@interface DSTrack : MTLModel <MTLJSONSerializing>

@property (nonatomic, strong) NSNumber *tid;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *title_short;
@property (nonatomic, strong) NSNumber *duration;
@property (nonatomic, strong) NSNumber *track_position;
@property (nonatomic, strong) NSNumber *disk_number;

@property (nonatomic, strong) DSArtist *artist;

@end
