//
//  DSTrackPresenter.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 04/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSTrackPresenter.h"
#import "DSTrackService.h"

@interface DSTrackPresenter ()

@property (weak, nonatomic) id <DSQueryView> queryView;
@property (strong, nonatomic) DSTrackService *service;

@end

@implementation DSTrackPresenter

- (instancetype)initWithQueryView:(id<DSQueryView>)queryView {
    return [super initWithQueryView:queryView service:[DSTrackService new]];
}

- (void)queryTracks:(NSString *)query {
    [super query:query];
}

@end
