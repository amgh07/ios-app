//
//  DSTrackPresenter.h
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 04/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSQueryPresenter.h"
#import "DSQueryView.h"

@interface DSTrackPresenter : DSQueryPresenter

- (instancetype)initWithQueryView:(id<DSQueryView>)queryView;
- (void)queryTracks:(NSString *)query;

@end
