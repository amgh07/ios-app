//
//  DSUtils.h
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 07/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DSUtils : NSObject

+ (BOOL)allowMergeOfStringInRange:(NSRange)range stringToMerge:(NSString *)stringToMerge stringAfterMerge:(NSString *)stringAfterMerge currentString:(NSString *)currentString;

@end
