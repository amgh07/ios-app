//
//  DSUtils.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 07/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSUtils.h"

@implementation DSUtils


+ (BOOL)allowMergeOfStringInRange:(NSRange)range stringToMerge:(NSString *)stringToMerge stringAfterMerge:(NSString *)stringAfterMerge currentString:(NSString *)currentString {
    
    //Check if stringToMerge is space and will be the only strig ater merge
    if (([stringToMerge rangeOfString:@" "].location != NSNotFound && [stringAfterMerge length] == 1)) {
        return NO;
    }
    //Check if a space follows a space
    if ((range.location > 0 && [stringToMerge length] > 0 &&
         [[NSCharacterSet whitespaceCharacterSet] characterIsMember:[stringToMerge characterAtIndex:0]] &&
         [[NSCharacterSet whitespaceCharacterSet] characterIsMember:[currentString characterAtIndex:range.location - 1]]) ) {
        return NO;
    }
    return YES;
}

@end
