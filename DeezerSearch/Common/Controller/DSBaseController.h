//
//  DSBaseController.h
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 06/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DSBaseController : UIViewController

- (void)dispatch_async_main_queue:(void (^)(void))completion;

@end
