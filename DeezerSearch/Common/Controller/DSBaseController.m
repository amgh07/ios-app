//
//  DSBaseController.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 06/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSBaseController.h"

@interface DSBaseController ()

@end

@implementation DSBaseController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor defaultColor]];
    [self.navigationController.navigationBar setTintColor:[UIColor whiteColor]];
    [self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"menu"] style:UIBarButtonItemStylePlain target:nil action:nil]];
    [self.navigationItem setLeftItemsSupplementBackButton:YES];
}
- (void)dispatch_async_main_queue:(void (^)(void))completion {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (completion) {
            completion();
        }
    });
}
@end
