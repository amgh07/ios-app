//
//  NSNumberFormatter+Additional.h
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 08/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumberFormatter (Additional)

+ (NSNumberFormatter *)trackFormatter;

@end
