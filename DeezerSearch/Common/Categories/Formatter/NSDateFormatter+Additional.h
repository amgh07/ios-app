//
//  NSDateFormatter+Additional.h
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 09/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDateFormatter (Additional)

+ (NSDateFormatter *)dateFormatter;

@end
