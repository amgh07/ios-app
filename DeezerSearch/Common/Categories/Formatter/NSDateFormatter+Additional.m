//
//  NSDateFormatter+Additional.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 09/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "NSDateFormatter+Additional.h"

@implementation NSDateFormatter (Additional)

+ (NSDateFormatter *)dateFormatter  {
    static NSDateFormatter *_dateFormatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _dateFormatter = [NSDateFormatter new];
        [_dateFormatter setDateFormat:@"mm:ss"];
    });
    return _dateFormatter;
}

@end
