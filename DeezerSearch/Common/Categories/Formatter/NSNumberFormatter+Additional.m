//
//  NSNumberFormatter+Additional.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 08/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "NSNumberFormatter+Additional.h"

@implementation NSNumberFormatter (Additional)

+ (NSNumberFormatter *)trackFormatter  {
    static NSNumberFormatter *formatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        formatter = [NSNumberFormatter new];
        [formatter setNumberStyle: NSNumberFormatterDecimalStyle];
    });
    return formatter;
}

@end
