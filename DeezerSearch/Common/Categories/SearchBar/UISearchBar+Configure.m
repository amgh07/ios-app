//
//  UISearchBar+Configure.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 06/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "UISearchBar+Configure.h"

@implementation UISearchBar (Configure)

- (UITextField *)textField {
    return [self valueForKey:@"searchField"];
}

@end
