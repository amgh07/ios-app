//
//  UITableView+Configure.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 04/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "UITableView+Configure.h"

@implementation UITableView (Configure)

- (void)setDefaultSeparatorAndLayoutMargin  {
    if ([self respondsToSelector:@selector(setSeparatorInset:)]) {
        [self setSeparatorInset:UIEdgeInsetsZero];
    }
    if ([self respondsToSelector:@selector(setLayoutMargins:)]) {
        [self setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)registerNibForArtistCell {
    [self registerNib:[UINib nibWithNibName:@"DSArtistCell" bundle:nil] forCellReuseIdentifier:@"CellIdentifier"];
}

- (void)registerNibForTrackCell {
    [self registerNib:[UINib nibWithNibName:@"DSTrackCell" bundle:nil] forCellReuseIdentifier:@"TrackCellIdentifier"];
}


@end
