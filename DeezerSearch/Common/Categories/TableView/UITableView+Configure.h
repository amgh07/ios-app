//
//  UITableView+Configure.h
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 04/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableView (Configure)

- (void)setDefaultSeparatorAndLayoutMargin;
- (void)registerNibForArtistCell;
- (void)registerNibForTrackCell;
@end
