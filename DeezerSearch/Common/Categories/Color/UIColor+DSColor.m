//
//  UIColor+DSColor.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 05/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "UIColor+DSColor.h"

@implementation UIColor (DSColor)

+ (UIColor *)defaultColor {
    static UIColor *_defaultColor;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _defaultColor = [UIColor colorWithRed:35.0f/255.0f green:35.0f/255.0f blue:35.0f/255.0f alpha:1.0f];
    });
    return _defaultColor;
}

+ (UIColor *)defaultLightColor {
    static UIColor *_defaultLightColor;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _defaultLightColor = [UIColor colorWithRed:45.0f/255.0f green:45.0f/255.0f blue:45.0f/255.0f alpha:1.0f];
    });
    return _defaultLightColor;
}

+ (UIColor *)defaultDarkColor {
    static UIColor *defaultDarkColor;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        defaultDarkColor = [UIColor colorWithRed:30.0f/255.0f green:30.0f/255.0f blue:30.f/255.0f alpha:1.0f];
        
    });
    return defaultDarkColor;
}
+ (UIColor *)secondaryLightColor {
    static UIColor *_secondaryLightColor;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _secondaryLightColor = [UIColor colorWithRed:164.0f/255.0f green:164.0f/255.0f blue:164.0f/255.0f alpha:1.0f];
    });
    return _secondaryLightColor;
}

@end
