//
//  UIColor+DSColor.h
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 05/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (DSColor)

+ (UIColor *)defaultColor;
+ (UIColor *)defaultLightColor;
+ (UIColor *)defaultDarkColor;
+ (UIColor *)secondaryLightColor;

@end
