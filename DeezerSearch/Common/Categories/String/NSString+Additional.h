//
//  NSString+Additional.h
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 06/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Additional)

- (NSString *)stringByTrimmingWhitespaceCharacterSet;
- (BOOL)lastCharacterIsMemberOfWhitespaceCharacterSet;
- (BOOL)isEmpty;
@end
