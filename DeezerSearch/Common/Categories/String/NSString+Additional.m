//
//  NSString+Additional.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 06/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "NSString+Additional.h"

@implementation NSString (Additional)

- (NSString *)stringByTrimmingWhitespaceCharacterSet {
    NSString *trimmedString = [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"  +" options:NSRegularExpressionCaseInsensitive error:nil];
    trimmedString = [regex stringByReplacingMatchesInString:trimmedString options:0 range:NSMakeRange(0, [trimmedString length]) withTemplate:@" "];
    return trimmedString;
}

- (BOOL)lastCharacterIsMemberOfWhitespaceCharacterSet {
    const char lastChar = [self characterAtIndex:[self length] - 1];
    if ([[NSCharacterSet whitespaceAndNewlineCharacterSet] characterIsMember:lastChar]) {
        return YES;
    }
    return NO;
}

- (BOOL)isEmpty {
    const char lastChar = [self characterAtIndex:[self length] - 1];
    if ([self isEqualToString:@""] && lastChar == 0) {
        return YES;
    }
    return NO;
}

@end
