//
//  DSQueryPresenter.h
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 09/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DSQueryView.h"
#import "DSQueryService.h"

@interface DSQueryPresenter : NSObject

- (instancetype)initWithQueryView:(id<DSQueryView>)artistView service:(DSQueryService *)service;

- (void)query:(NSString *)query newQuery:(BOOL)newQuery;
- (void)query:(NSString *)query;

- (void)cancelAllPendingQueries;


@end
