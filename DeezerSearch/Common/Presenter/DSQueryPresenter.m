//
//  DSQueryPresenter.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 09/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSQueryPresenter.h"
#import "DSQueryService.h"

@interface DSQueryPresenter ()

@property (weak, nonatomic) id <DSQueryView> queryView;
@property (strong, nonatomic) DSQueryService *service;
@property (assign, nonatomic) BOOL cancelQueries;

@end

@implementation DSQueryPresenter



- (instancetype)initWithQueryView:(id<DSQueryView>)queryView service:(DSQueryService *)service {
    self = [super init];
    if (self) {
        [self setQueryView:queryView];
        [self setService:service];
    }
    return self;
}

- (void)query:(NSString *)query newQuery:(BOOL)newQuery {
    
    [self viewWillQuery];
    
    [self setCancelQueries:NO];
    
    [self.service query:query newQuery:newQuery completion:^(NSArray *results, DSAPIResponseState state) {
        
        [self viewDidQuery];
        
        if (results && results.count >= 1 && !self.cancelQueries) {
            [self.queryView viewDidQueryWithResult:results append:!newQuery];
            return;
        }
        [self handleResponseState:state];
    }];
}

- (void)query:(NSString *)query {
    
    [self viewWillQuery];
    
    [self.service query:query completion:^(NSArray *tracks, DSAPIResponseState state, BOOL hasNext) {
        
        [self viewDidQuery];
        
        if (tracks && tracks.count >= 1) {
            [self.queryView viewDidQueryWithResult:tracks append:hasNext];
            if (hasNext) {
                [self query:query];
            }
            return;
        }
        [self handleResponseState:state];
    }];
}

- (void)handleResponseState:(DSAPIResponseState)state {
    switch (state) {
        case DSAPIResponseStateCanceled: {
        }
            break;
        case DSAPIResponseStateDisconnected: {
            if ([self.queryView respondsToSelector:@selector(viewDidFailedToQuery)]) {
                [self.queryView viewDidFailedToQuery];
            }
        }
            break;
        case DSAPIResponseStateNoContent: {
            if ([self.queryView respondsToSelector:@selector(viewDidReachQueryLimit)]) {
                [self.queryView viewDidReachQueryLimit];
            }
        }
            break;
        case DSAPIResponseStateOK: {
            if ([self.queryView respondsToSelector:@selector(viewDidQueryEmptyResult)]) {
                [self.queryView viewDidQueryEmptyResult];
            }
        }
            break;
    }
}

- (void)viewDidQuery {
    if ([self.queryView respondsToSelector:@selector(viewDidQuery)]) {
        [self.queryView viewDidQuery];
    }
}

- (void)viewWillQuery {
    if ([self.queryView respondsToSelector:@selector(viewWillQuery)]) {
        [self.queryView viewWillQuery];
    }
}

- (void)cancelAllPendingQueries {
    [self setCancelQueries:YES];
}

@end
