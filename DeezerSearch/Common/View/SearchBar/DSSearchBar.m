//
//  DSSearchBar.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 05/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSSearchBar.h"

@implementation DSSearchBar

- (void)awakeFromNib {
    [super awakeFromNib];

    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [UIColor defaultColor].CGColor);
    CGContextFillRect(context, rect);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //Make sure to hide the borders!
    [self setBackgroundImage:image];
    
    UITextField *searchField = [self valueForKey:@"searchField"];
    searchField.textColor = [UIColor whiteColor];
    
    [self setBarStyle:UIBarStyleBlack];
    [self setKeyboardAppearance:UIKeyboardAppearanceDark];
}
@end
