//
//  DSPlaceholderView.h
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 04/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, DSEmptyViewType) {
    DSEmptyViewTypeHappy = 0,
    DSEmptyViewTypeSad,
    DSEmptyViewTypeSearch,
    DSEmptyViewTypeConnection,
    DSEmptyViewTypeFailure
};

@interface DSPlaceholderView : UIView

- (void)configureWithViewType:(DSEmptyViewType)type message:(NSString *)message;

@end
