//
//  DSPlaceholderView.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 04/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSPlaceholderView.h"

@interface DSPlaceholderView ()

@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIImageView *statusImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation DSPlaceholderView

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initView];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initView];
    }
    return self;
}

- (void)initView {
    
    [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
    [self addSubview:self.contentView];
    
    self.contentView.frame = self.bounds;
}

- (void)configureWithViewType:(DSEmptyViewType)type message:(NSString *)message {
    NSString *imageName = @"happy";
    switch (type) {
        case DSEmptyViewTypeSad:
            imageName = @"sad";
            break;
        case DSEmptyViewTypeConnection:
            imageName = @"noConnection";
            break;
        case DSEmptyViewTypeFailure:
            break;
        case DSEmptyViewTypeSearch:
            imageName = @"smile";
            break;
        case DSEmptyViewTypeHappy:
          default:
            break;
    }
    [UIView transitionWithView:self.statusImageView
                      duration:0.4f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        [self.statusImageView setImage:[UIImage imageNamed:imageName]];
                    } completion:NULL];
        [self.titleLabel setText:message];
}


@end
