//
//  DSQueryView.h
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 09/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

@protocol DSQueryView <NSObject>

// Called when the implementer have loaded
- (void)viewDidQueryWithResult:(NSArray *)result append:(BOOL)append;

@optional
// Called when the implementer is about to query
- (void)viewWillQuery;

// Called when the implementer is done query
- (void)viewDidQuery;

// Called when the view have loded artists, but the COUNT is 0
- (void)viewDidQueryEmptyResult;

// Called when the view have received all results
- (void)viewDidReachQueryLimit;

// Called when the view where unable to query artists
- (void)viewDidFailedToQuery;

@end

