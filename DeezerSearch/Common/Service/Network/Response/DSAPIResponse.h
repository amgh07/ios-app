//
//  DSAPIResponse.h
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 04/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DSResponseState.h"

@interface DSAPIResponse : NSObject

- (_Nonnull instancetype)initWithData:(NSData * _Nullable)data response:(NSURLResponse * _Nullable)response error:(NSError * _Nullable)error;

@property (assign, nonatomic) DSAPIResponseState state;

- (NSDictionary * _Nullable)result;
- (NSError * _Nullable)error;

- (BOOL)isDisconnected;
- (BOOL)isOK; 
- (BOOL)isCanceled;



@end
