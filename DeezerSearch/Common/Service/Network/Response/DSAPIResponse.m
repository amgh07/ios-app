//
//  DSAPIResponse.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 04/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSAPIResponse.h"

@interface DSAPIResponse ()

@property (strong, nonatomic) NSError *error;
@property (strong, nonatomic) NSHTTPURLResponse *response;
@property (strong, nonatomic) NSDictionary *result;

@end

@implementation DSAPIResponse

#pragma mark - Init

- (instancetype)initWithData:(NSData *)data response:(NSURLResponse *)response error:(NSError *)error {
    self = [super init];
    if (self) {
        self.error = error;
        if (!error) {
            self.result = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
        }
        if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
            self.response = (NSHTTPURLResponse *)response;
        }
    }
    
    return self;
}

#pragma mark - Additional

- (BOOL)isDisconnected {
    return (self.error.code == -1009 && ([self.error domain] == NSURLErrorDomain || [[self.error domain] isEqualToString:NSURLErrorDomain]));
}

- (BOOL)isOK {
    return (self.response.statusCode <= 300 && self.response.statusCode >= 200);
}

- (BOOL)isCanceled {
    return (self.error.code == -999 && ([self.error domain] == NSURLErrorDomain || [[self.error domain] isEqualToString:NSURLErrorDomain]));
}

- (BOOL)noContent {
    return (self.response.statusCode == 204);
}

- (DSAPIResponseState)state {
    if ([self isDisconnected]) {
        return DSAPIResponseStateDisconnected;
    } else if ([self isOK]) {
        return DSAPIResponseStateOK;
    } else if ([self noContent]) {
        return DSAPIResponseStateNoContent;
    }
    return DSAPIResponseStateCanceled;
}

@end
