//
//  DSAPIGETRequest.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 05/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSAPIGETRequest.h"

@implementation DSAPIGETRequest

- (void)dataTaskInBackgroundWithPath:(NSString *)path completion:(GetRequestCompletionHandler)completion {
    [super dataTaskInBackgroundWithPath:path method:kDSHTTPMethodGet completion:completion];
}
- (NSURLSessionDataTask * _Nonnull)dataTaskWithPath:(NSString * _Nonnull)path completion:(_Nonnull GetRequestCompletionHandler)completion {
    return [super dataTaskWithPath:path method:kDSHTTPMethodGet completion:completion];
}

@end
