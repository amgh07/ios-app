//
//  DSAPIGETRequest.h
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 05/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSAPIRequest.h"

typedef void (^GetRequestCompletionHandler)(DSAPIResponse * _Nonnull response);

@interface DSAPIGETRequest : DSAPIRequest

- (void)dataTaskInBackgroundWithPath:(NSString * _Nonnull)path completion:(_Nonnull GetRequestCompletionHandler)completion;
- (NSURLSessionDataTask * _Nonnull)dataTaskWithPath:(NSString * _Nonnull)path completion:(_Nonnull GetRequestCompletionHandler)completion;

@end
