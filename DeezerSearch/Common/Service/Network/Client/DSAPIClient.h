//
//  DSAPIClient.h
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 04/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^ApiClientCompletionHandler)(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error);

@interface DSAPIClient : NSObject

- (_Nonnull instancetype)initWithBaseURL:(NSURL * _Nonnull)baseURL;
- (_Nonnull instancetype)initWithSession:(NSURLSession * _Nullable)session baseURL:(NSURL * _Nonnull)baseURL;

- (void)dataTaskInBackgroundWithPath:(NSString * _Nonnull)path method:(NSString * _Nonnull)method completion:(_Nonnull ApiClientCompletionHandler)completion;
- (NSURLSessionDataTask * _Nonnull)dataTaskWithPath:(NSString * _Nonnull)path method:(NSString * _Nonnull)method completion:(_Nonnull ApiClientCompletionHandler)completion;

@end
