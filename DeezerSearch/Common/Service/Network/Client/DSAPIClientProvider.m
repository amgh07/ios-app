//
//  DSAPIClientProvider.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 08/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSAPIClientProvider.h"

@interface DSAPIClientProvider ()

@property (strong, nonatomic) DSAPIClient *client;

@end

@implementation DSAPIClientProvider

+ (DSAPIClientProvider *)sharedProvider {
    
    static DSAPIClientProvider *_sharedProvider;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedProvider = [DSAPIClientProvider new];
        _sharedProvider->_client = [[DSAPIClient alloc] initWithSession:[_sharedProvider decoratedSession] baseURL:[NSURL URLWithString: @"https://api.deezer.com/"]];
    });
    
    return _sharedProvider;
}

- (DSAPIClient *)client {
    if (!_client) {
            _client = [[DSAPIClient alloc] initWithSession:[self decoratedSession] baseURL:[NSURL URLWithString: @"https://api.deezer.com/"]];
    }
    return _client;
}

- (NSURLSession *)decoratedSession {
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    [configuration setHTTPAdditionalHeaders:@{ @"Accept" : @"application/json; charset=utf-8" }];
    
    return [NSURLSession sessionWithConfiguration:configuration];
}

@end
