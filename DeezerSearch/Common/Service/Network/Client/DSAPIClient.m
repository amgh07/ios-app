//
//  DSAPIClient.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 04/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSAPIClient.h"

@interface DSAPIClient()

@property (strong, nonatomic) NSURLSession *session;
@property (strong, nonatomic) NSURL *baseURL;

@end

@implementation DSAPIClient

#pragma mark - Init

- (instancetype)initWithBaseURL:(NSURL *)baseURL {
    return [self initWithSession:nil baseURL:baseURL];
}

- (instancetype)initWithSession:(NSURLSession *)session baseURL:(NSURL *)baseURL {
    self = [super init];
    if (self) {
        self.session = session;
        self.baseURL = baseURL;
    }
    
    return self;
}

#pragma mark - Session

- (NSURLSession *)session {
    if (!_session) {
        _session = [self decoratedSession];
    }
    return _session;
}

- (NSURLSession *)decoratedSession {
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    [configuration setHTTPAdditionalHeaders:@{ @"Accept" : @"application/json; charset=utf-8" }];
    
    return [NSURLSession sessionWithConfiguration:configuration];
}

#pragma mark - DataTask

- (void)dataTaskInBackgroundWithPath:(NSString *)path method:(NSString *)method completion:(ApiClientCompletionHandler)completion {
    
    NSURLSessionDataTask *dataTask = [self dataTaskWithPath:path method:method completion:completion];
    
    [dataTask resume];
}

- (NSURLSessionDataTask *)dataTaskWithPath:(NSString *)path method:(NSString *)method completion:(ApiClientCompletionHandler)completion {
    
    NSMutableURLRequest *request = [self requestWithPath:path method:method];
    NSURLSessionDataTask *dataTask = [[self session] dataTaskWithRequest:request completionHandler:completion];
    
    return dataTask;
}

#pragma mark - NSURL Helper

- (NSMutableURLRequest *)requestWithPath:(NSString *)path method:(NSString *)method {
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[self requestURLWithPath:path] cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20.f];
    [request setHTTPMethod:method];
    
    return request;
}

- (NSURL *)requestURLWithPath:(NSString *)path {
    return [NSURL URLWithString:[[path stringByRemovingPercentEncoding] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]] relativeToURL:self.baseURL];
}

#pragma mark - Dealloc

- (void)dealloc {
    if (_session) {
        [_session invalidateAndCancel];
        _session = nil;
    }
}

@end
