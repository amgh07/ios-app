//
//  DSAPIConstant.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 05/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSAPIConstant.h"

@implementation DSAPIConstant

NSString *const kDSBaseApiUrl = @"https://api.deezer.com/";
NSString *const kDSSearchApiUrl = @"search/";
NSString *const kDSTrackApiUrl = @"track";
NSString *const kDSAlbumApiUrl = @"album/";

NSString *const kDSHTTPMethodGet = @"GET";

@end
