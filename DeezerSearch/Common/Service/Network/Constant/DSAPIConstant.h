//
//  DSAPIConstant.h
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 05/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DSAPIConstant : NSObject

extern NSString *const kDSBaseApiUrl;
extern NSString *const kDSSearchApiUrl;
//extern NSString *const kDSArtistApiUrl;
extern NSString *const kDSAlbumApiUrl;
extern NSString *const kDSTrackApiUrl;

extern NSString *const kDSHTTPMethodGet;

@end
