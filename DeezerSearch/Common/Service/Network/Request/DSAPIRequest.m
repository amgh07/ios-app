//
//  DSAPIRequest.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 04/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSAPIRequest.h"
#import "DSAPIClient.h"
#import "DSAPIClientProvider.h"

@interface DSAPIRequest ()

@property (strong, nonatomic) DSAPIClient *client;

@end

@implementation DSAPIRequest

#pragma mark - Init

- (instancetype)init {
    return [self initWithSession:nil];
}

- (instancetype)initWithSession:(NSURLSession *)session {
    
    self = [super init];
    if (self) {
        [self setClient:[DSAPIClientProvider sharedProvider].client];
    }
    return self;
}

#pragma mark - DataTask

- (void)dataTaskInBackgroundWithPath:(NSString * _Nonnull)path method:(NSString * _Nonnull)method completion:(_Nonnull ApiRequestCompletionHandler)completion {
    
    [self.client dataTaskInBackgroundWithPath:path method:method completion:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        completion([[DSAPIResponse alloc] initWithData:data response:response error:error]);
    }];
}

- (NSURLSessionDataTask * _Nonnull)dataTaskWithPath:(NSString * _Nonnull)path method:(NSString * _Nonnull)method completion:(_Nonnull ApiRequestCompletionHandler)completion {
    
    return [self.client dataTaskWithPath:path method:method completion:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        completion([[DSAPIResponse alloc] initWithData:data response:response error:error]);
    }];
}


@end
