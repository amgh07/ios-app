//
//  DSAPIRequest.h
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 04/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DSAPIResponse.h"
#import "DSAPIConstant.h"



typedef void (^ApiRequestCompletionHandler)(DSAPIResponse * _Nonnull response);

@interface DSAPIRequest : NSObject

- (void)dataTaskInBackgroundWithPath:(NSString * _Nonnull)path method:(NSString * _Nonnull)method completion:(_Nonnull ApiRequestCompletionHandler)completion;
- (NSURLSessionDataTask * _Nonnull)dataTaskWithPath:(NSString * _Nonnull)path method:(NSString * _Nonnull)method completion:(_Nonnull ApiRequestCompletionHandler)completion;

@end
