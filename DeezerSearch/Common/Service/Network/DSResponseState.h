//
//  SharedState.h
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 06/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#ifndef SharedState_h
#define SharedState_h

typedef NS_ENUM(NSInteger, DSAPIResponseState) {
    DSAPIResponseStateOK = 0,
    DSAPIResponseStateCanceled,
    DSAPIResponseStateDisconnected,
    DSAPIResponseStateNoContent = 204
};

#endif /* SharedState_h */
