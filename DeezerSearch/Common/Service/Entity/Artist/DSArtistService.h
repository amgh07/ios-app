//
//  DSArtistService.h
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 05/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSQueryService.h"

typedef void (^ArtistQueryCompletionHandler)(NSArray *artists, DSAPIResponseState state);

@interface DSArtistService : DSQueryService

- (void)queryArtists:(NSString *)query newQuery:(BOOL)newQuery completion:(ArtistQueryCompletionHandler)completion;

@end
