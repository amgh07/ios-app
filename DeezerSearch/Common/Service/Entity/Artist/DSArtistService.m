//
//  DSArtistService.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 05/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSArtistService.h"
#import "DSArtist.h"

@implementation DSArtistService

- (instancetype)init {
    return [super initWithPath:@"artist" model:[DSArtist class]];
}

- (void)queryArtists:(NSString *)query newQuery:(BOOL)newQuery completion:(ArtistQueryCompletionHandler)completion {
    [super query:query newQuery:newQuery completion:completion];
}

@end
