//
//  DSAlbumService.h
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 05/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSQueryService.h"

typedef void (^AlbumQueryCompletionHandler)(NSArray *albums, DSAPIResponseState state);

@interface DSAlbumService : DSQueryService

- (void)queryAlbums:(NSString *)query newQuery:(BOOL)newQuery completion:(AlbumQueryCompletionHandler)completion;

@end
