//
//  DSAlbumService.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 05/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSAlbumService.h"
#import "DSAlbum.h"

@implementation DSAlbumService

- (instancetype)init {
    return [super initWithPath:@"album" model:[DSAlbum class]];
}

- (void)queryAlbums:(NSString *)query newQuery:(BOOL)newQuery completion:(AlbumQueryCompletionHandler)completion {
    [super query:query newQuery:newQuery completion:completion];
}

@end
