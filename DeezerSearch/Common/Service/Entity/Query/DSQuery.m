//
//  DSQuery.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 05/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSQuery.h"

@implementation DSQuery

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"next": @"next",
             @"total": @"total",
             @"json": @"data",
             @"prev": @"prev"
             };
}


- (NSString *)nextQuery {
    NSArray *nextQuery = [self.next componentsSeparatedByString:@"?"];
    return [NSString stringWithFormat:@"?%@", [nextQuery lastObject]];
}
- (BOOL)hasNextUrl {
    return self.next != nil;
}

- (BOOL)hasReachedLimit {
    return self.next == nil && self.prev != nil;
}
@end
