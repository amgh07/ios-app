//
//  DSQuery.h
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 05/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface DSQuery : MTLModel <MTLJSONSerializing>

@property (strong, nonatomic) NSString *next;
@property (strong, nonatomic) NSString *prev;
@property (strong, nonatomic) NSArray *json;
@property (strong, nonatomic) NSNumber *total;

- (NSString *)nextQuery;
- (BOOL)hasNextUrl;
- (BOOL)hasReachedLimit;

@end
