//
//  DSQueryService.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 07/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSQueryService.h"
#import "DSModelMapper.h"
#import "DSQuery.h"
#import "DSAPIGETRequest.h"
#import "DSArtist.h"

@interface DSQueryService ()

@property (strong, nonatomic) DSAPIGETRequest *request;
@property (strong, nonatomic) DSModelMapper *modelMapper;
@property (strong, nonatomic) DSQuery *lastQuery;
@property (strong, nonatomic) NSURLSessionDataTask *queryDataTask;
@property (strong, nonatomic) NSString *path;
@property (strong, nonatomic) id model;

@end

@implementation DSQueryService

- (instancetype)initWithPath:(NSString *)path model:(id)model {
    self = [super init];
    if (self) {
        [self setRequest:[DSAPIGETRequest new]];
        [self setPath:path];
        [self setModel:model];
    }
    return self;
}

- (DSModelMapper *)modelMapper {
    if (!_modelMapper) {
        _modelMapper = [DSModelMapper new];
    }
    
    return _modelMapper;
}

- (void)query:(NSString *)query newQuery:(BOOL)newQuery completion:(QueryCompletionHandler)completion {

    //Marks the old task as cancel!
    if (self.queryDataTask) {
        [self.queryDataTask cancel];
    }
    
    if (newQuery) {
        query = [NSString stringWithFormat:@"?q=%@", query];
    } else if (!newQuery && [self.lastQuery hasNextUrl]) {
        query = [NSString stringWithFormat:@"%@", [self.lastQuery nextQuery]];
    } else {
        completion(nil, DSAPIResponseStateNoContent);
        return;
    }
    
    self.queryDataTask = [self.request dataTaskWithPath:[self pathForQuery:query] completion:^(DSAPIResponse * _Nonnull response) {
        DSQuery *queryResult =  [self queryResult:[response result]];
    
        //Update the last query for later check
        [self setLastQuery:queryResult];
        completion([self queryArray:queryResult.json], [response state]);
    }];
    
    //Resume the task if not canceled
    if (self.queryDataTask) {
        [self.queryDataTask resume];
    }
}


- (void)query:(NSString *)query completion:(SearchCompletionHandler)completion  {
    
    if ([self.lastQuery hasNextUrl]) {
        [self setPath:[NSString stringWithFormat:@"%@%@", self.path, [self.lastQuery nextQuery]]];
    }
    
    [self.request dataTaskInBackgroundWithPath:[self pathForSearch:query] completion:^(DSAPIResponse * _Nonnull response) {
        DSQuery *queryResult =  [self queryResult:[response result]];
        [self setLastQuery:queryResult];
        completion([self queryArray:queryResult.json], [response state], [self.lastQuery hasNextUrl]);
    }];
}

- (DSQuery *)queryResult:(NSDictionary *)result {
    return [self.modelMapper modelOfClassFromJSONDictionary:result model:DSQuery.class];
}

- (NSArray *)queryArray:(NSArray *)array {
    return [self.modelMapper modelsOfClassFromJSONArray:array model:[self.model class]];
}

- (NSString *)pathForQuery:(NSString *)query {
    return [NSString stringWithFormat:@"%@%@%@", kDSSearchApiUrl, self.path, query];
}

-(NSString *)pathForSearch:(NSString *)search {
    return [NSString stringWithFormat:@"%@%@%@", kDSAlbumApiUrl, search, self.path];
}

@end
