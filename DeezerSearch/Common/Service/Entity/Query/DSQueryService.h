//
//  DSQueryService.h
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 07/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DSResponseState.h"
#import "DSAPIConstant.h"

typedef void (^QueryCompletionHandler)(NSArray *result, DSAPIResponseState state);
typedef void (^SearchCompletionHandler)(NSArray *result, DSAPIResponseState state, BOOL hasNext);

@interface DSQueryService : NSObject

- (instancetype)initWithPath:(NSString *)path model:(id)model;
- (void)query:(NSString *)query newQuery:(BOOL)newQuery completion:(QueryCompletionHandler)completion;
- (void)query:(NSString *)query completion:(SearchCompletionHandler)completion;

@end
