//
//  DSTrackService.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 05/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSTrackService.h"
#import "DSTrack.h"

@implementation DSTrackService

- (instancetype)init {
    return [super initWithPath:@"/tracks" model:[DSTrack class]];
}

- (void)queryTracks:(NSString *)query completion:(TrackQueryCompletionHandler)completion {
    [super query:query completion:completion];
}

@end
