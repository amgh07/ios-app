//
//  DSTrackService.h
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 05/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSQueryService.h"

typedef void (^TrackQueryCompletionHandler)(NSArray *result, DSAPIResponseState state, BOOL hasNext);

@interface DSTrackService : DSQueryService

- (void)queryTracks:(NSString *)query completion:(TrackQueryCompletionHandler)completion;

@end
