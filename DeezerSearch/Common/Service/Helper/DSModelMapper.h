//
//  DSModelMapper.h
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 05/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DSModelMapper : NSObject

- (_Nullable id)modelOfClassFromJSONDictionary:(NSDictionary * _Nonnull)dictionary model:(id _Nonnull)model;
- (NSArray * _Nonnull)modelsOfClassFromJSONArray:(NSArray * _Nonnull)array model:(id _Nonnull)model;

@end
