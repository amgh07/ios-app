//
//  DSModelMapper.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 05/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "DSModelMapper.h"
#import <Mantle/Mantle.h>

@implementation DSModelMapper

- (id)modelOfClassFromJSONDictionary:(NSDictionary *)dictionary model:(id)model {
    
    id modelOfClass = nil;
    modelOfClass = [MTLJSONAdapter modelOfClass:[model class] fromJSONDictionary:dictionary error:nil];

    return modelOfClass;
}

- (NSArray *)modelsOfClassFromJSONArray:(NSArray *)array model:(id)model {
    
    NSMutableArray *models = [NSMutableArray array];
    
    for (NSDictionary *item in array) {
        id modelOfClass = [self modelOfClassFromJSONDictionary:item model:model];
        if (modelOfClass) {
            [models addObject:modelOfClass];
        }
    }
    
    return [NSArray arrayWithArray:models];;
}

@end
