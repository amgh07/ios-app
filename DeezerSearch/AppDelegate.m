//
//  AppDelegate.m
//  DeezerSearch
//
//  Created by Amir Ghoreshi on 04/10/2017.
//  Copyright © 2017 Amir Ghoreshi. All rights reserved.
//

#import "AppDelegate.h"
#import "DSArtistController.h"

#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    [Fabric with:@[[Crashlytics class]]];

    [self setWindow:[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]]];
    
    DSArtistController *artistController = [DSArtistController new];
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:artistController];
    navigationController.navigationBar.barStyle = UIBarStyleBlack;
    
    [[UINavigationBar appearance] setTranslucent:NO];
    [[UINavigationBar appearance] setBarTintColor:[UIColor defaultColor]];
    [[UINavigationBar appearance] setBackgroundImage:[UIImage new]forBarMetrics: UIBarMetricsDefault];
    [UINavigationBar appearance].shadowImage = [UIImage new];

    [self.window setRootViewController:navigationController];
    
    [self.window makeKeyAndVisible];
    
    return YES;
}

@end
